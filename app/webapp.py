#!/usr/bin/env python3

from flask import Flask, render_template
from blairdo import *
from datetime import datetime

app = Flask(__name__)

@app.route('/')
@app.route('/index')
def index():

    ds = get_droplets()

    if ds is None:
        return "Is your API key loaded?"

    bigdroplets = [ d for d in ds if d['size_slug'] not in ("s-1vcpu-2gb", "s-1vcpu-1gb") ]
    droplets    = [ d for d in ds if d['size_slug']     in ("s-1vcpu-2gb", "s-1vcpu-1gb") ]
    
    return render_template(
        'index.html',
        billing = get_billing(),
        droplets = droplets,
        volumes = get_volumes(),
        k8s = get_k8s(),
        bigdroplets = bigdroplets,
        time=datetime.now().strftime('%H:%M:%S, %Y-%m-%d')
    )

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=8000)
